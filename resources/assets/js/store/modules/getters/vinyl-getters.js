export default {
    sides: state => {
        let songs = state.songs;

        let sides = [];
        let track = 1;
        for (var i in songs) {
            let song_path = songs[i];
            let startIndex = (song_path.indexOf('\\') >= 0 ? song_path.lastIndexOf('\\') : song_path.lastIndexOf('/'));
            let filename = song_path.substring(startIndex);
            if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                filename = filename.substring(1);
            };

            let side = 'a';
            if (i > 6) {
                side = 'b';
                track = 1;
            }

            let song = {
                'path': song_path,
                'side': side,
                'track': track,
                'name': filename
            }

            sides.push(song);

            track++;
        }

        return sides;
    }
}