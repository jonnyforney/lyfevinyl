@extends('layouts.app')

@section('content')

  <div id="app">
    <div class="row">
      <div class="col-md-12">
        <div id="progress-bar" v-cloak>
          <progress-bar percent="16.667"></progress-bar>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 margin-top-30--medium-up margin-bottom-50--medium-up">
        <div id="steps" v-cloak>
          <steps
            customer_id="{{ session('customer_id') }}"
            order="{{ session('order')['id'] }}"
            is_logged_in="{{ Auth::check() }}"
            stripe_token="{{ config('services.stripe.key') }}"
          ></steps>
        </div>
      </div>
    </div>
  </div>

@endsection
